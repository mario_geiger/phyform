all: formulaire.pdf

formulaire.pdf: *.tex
	pdflatex formulaire.tex

clean:
	rm -f *.aux *.log *.pdf
