\graysec{QFT}


\graypar{Notation}
\squishlist
\item[] $d\Omega_k = \frac{d^3 k}{(2\pi)^3} \frac{1}{2 k_0}$
\item[] $xk = x_\mu k^\mu$
\item[] $k_0 = \sqrt{\vec k^2 + m^2}$
\squishend

\graypar{Math}
\squishlist
\item[] $\epsilon^{ijk}\epsilon^{ijl} = 2 \delta^{kl}$
\item[] $\epsilon^{kij}\epsilon^{klm} = \delta^{il} \delta^{jm} - \delta^{im} \delta^{jl}$
\squishend

\graypar{Noether}
\squishlist
\item[] $\phi(x) \longrightarrow \phi'(x) = \phi(x) + \alpha \ \delta \phi(x)$
\item[] $\mathcal L[\phi', \partial \phi'](x) - \mathcal L[\phi, \partial \phi](x) = \alpha \partial_\mu J^\mu(x) + \alpha \Delta(x)$
\item[] $\partial_\mu j^\mu = \Delta - \frac{\delta S}{\delta \phi} \, \delta \phi$ where $j^\mu = \frac{\partial \mathcal L}{\partial (\partial_\mu \phi)} \delta \phi - J^\mu$
\squishend

\graypar{Lorentz}
\squishlist
\item[] $\Lambda^\mu_{\;\;\nu} (\vec \eta, \vec \theta) \equiv \exp  \left(\!\begin{smallmatrix}
0      & \eta^1 & \eta^2 & \eta^3 \\
\eta^1 & 0 & -\theta^3 & \theta^2 \\
\eta^2 & \theta^3 & 0 & -\theta^1 \\
\eta^3 & -\theta^2 & \theta^1 & 0 \\
\end{smallmatrix}\!\right)$ spans $SO(1,3)$
\squishend

\graypar{Klein-Gordon}
\squishlist
\item[] $\mathcal L = \frac1{2} (\partial \phi)^2 - \frac{m}{2} \phi^2$
\item[] $\pi = \partial_0 \phi \Rightarrow H = \frac1{2} \pi^2 + \partial_i \phi \partial_i \phi + \frac{m}{2} \phi^2 \Rightarrow [\phi(\vec x,t), \pi(\vec y,t)] = i \delta^3(\vec x - \vec y)$
\item[] $\phi(\vec x, t) = \int \frac{d^3k}{(2\pi)^3} e^{i \vec k \vec x} \phi(\vec k,t)$
\item[] $a(\vec k) = k_0 \phi(\vec k) + i \pi(\vec k) \Rightarrow [a(\vec k), a^\dagger(\vec p)] = (2\pi)^3 2 k_0 \delta^3(\vec k - \vec p)$
\item[] $\phi(x^\mu) = \int \frac{d^3 k}{(2\pi)^3 2 k_0} \left(a(\vec k) e^{-i k_\mu x^\mu} + a^\dagger(\vec k) e^{i k_\mu x^\mu}\right)$
\squishend

\graypar{Spinors}
\squishlist
\item[] $\sigma^0 = \left(\!\begin{smallmatrix}
1 & 0 \\
0 & 1 \end{smallmatrix}\!\right)
\;\sigma^1 =\left(\!\begin{smallmatrix}
0 & 1 \\
1 & 0 \end{smallmatrix}\!\right)
\;\sigma^2 =\left(\!\begin{smallmatrix}
0 & -i \\
i & 0 \end{smallmatrix}\!\right)
\;\sigma^3 =\left(\!\begin{smallmatrix}
1 & 0 \\
0 & -1 \end{smallmatrix}\!\right)
\;\epsilon =\left(\!\begin{smallmatrix}
0 & 1 \\
-1 & 0 \end{smallmatrix}\!\right)$
\item[] $\sigma^{\mu\dagger} = \sigma^\mu \quad (\sigma^\mu)^2 = 1$ 
\item[] $[\sigma^i, \sigma^j] = 2i \epsilon^{ijk} \sigma^k$
\item[] $\{\sigma^i, \sigma^j\} = 2 \delta^{ij}$
\item[] $\sigma_\mu \bar \sigma_\nu + \sigma_\nu \bar \sigma_\mu = 2 \eta_{\mu \nu} \quad \Rightarrow X_\mu \sigma^\mu X_\nu \bar \sigma^\nu = X_\mu X^\mu = \det(X_\mu \sigma^\mu)$
\item[] $\epsilon \sigma^\mu \epsilon^T = \bar\sigma^{\mu*}$
\item[] $\Lambda_L \equiv \exp(-\frac1{2} (\eta^i + i \theta^i) \sigma^i)$ spans $SL(2, \mathbb C)$
\item[] $\Lambda_R \equiv \exp(-\frac1{2} (-\eta^i + i \theta^i) \sigma^i)$
\item[] $\epsilon \Lambda_L \epsilon^T = \Lambda_R^* \quad \Leftrightarrow \quad \epsilon \Lambda_R \epsilon^T = \Lambda_L^*$
\item[] $\begin{array}{ll}\Lambda_L^\dagger \bar \sigma_\mu \Lambda_L = \Lambda_\mu^{\;\;\nu} \bar \sigma_\nu
&\; \Leftrightarrow \; \Lambda_R^\dagger \sigma_\mu \Lambda_R = \Lambda_\mu^{\;\;\nu} \sigma_\nu 
\\\; \Leftrightarrow \; \Lambda_L \sigma^\mu \Lambda_L^\dagger = \Lambda_\nu^{\;\;\mu} \sigma^\nu 
&\; \Leftrightarrow \; \Lambda_R \bar \sigma^\mu \Lambda_R^\dagger = \Lambda_\nu^{\;\;\mu} \bar \sigma^\nu
\end{array}$
\item[] $\exp(A \otimes 1 + 1 \otimes B) = \exp(A) \otimes \exp(B)$
\item[] $(1/2,0) \rightarrow \Lambda_L$ truly speaking it's not a representation of the proper othochronous Lorentz group because $\Lambda_L(\text{half-turn}) \Lambda_L(\text{half-turn}) = -1 \neq \Lambda_L(\text{full-turn})$. It is a representation of its covering group : $SL(2, \mathbb C)$
\item[] $(0,1/2) \rightarrow \Lambda_R$
\item[] $(1/2,0)+(0,1/2) \rightarrow \left(\!\begin{smallmatrix}
\Lambda_L & 0 \\
0 & \Lambda_R \end{smallmatrix}\!\right)
$
\item[] $(1/2,1/2) \rightarrow \Lambda_L \otimes \Lambda_R$
\item[] $(\epsilon \otimes \epsilon) (\Lambda_L \otimes \Lambda_L^*) (\epsilon \otimes \epsilon)^{-1} = (\Lambda_R^* \otimes \Lambda_R)$
\item[] ${\rm Partity} (\Lambda_L \otimes \Lambda_L^*) {\rm Partity} = (\Lambda_R \otimes \Lambda_R^*)$
\squishend

\graypar{Dirac}
\squishlist
\item[] $\gamma^\mu = \left(\!\begin{smallmatrix}
0 & \sigma^\mu \\
\bar \sigma^\mu & 0 \end{smallmatrix}\!\right) \quad \gamma^0 \gamma^\mu \gamma^0 = \gamma_\mu$
\item[] $\psi = \left(\!\begin{smallmatrix} \psi_L \\
\psi_R \end{smallmatrix}\!\right)$
\item[] $\bar \psi = \psi^\dagger \gamma^0$
\squishend

\graypar{Vecteur}
\squishlist
\item[] $F^{\mu\nu} = \partial^\mu A^\nu - \partial^\nu A^\mu = 
\left(\!\begin{smallmatrix}
0      & -E^1 & -E^2 & -E^3 \\
 & 0 & -B^3 & B^2 \\
 &  & 0 & -B^1 \\
 &  &  & 0 \\
\end{smallmatrix}\!\right)\;
\begin{array}{l}
A^\mu = (\phi \; \vec A) \\
E^i = - \partial_i \phi - \partial_0 A^i \\
B^i = \epsilon^{ijk} \partial_j A^k
\end{array}$
\squishend

\graypar{Gupta-Bleuler}
\squishlist
\item[] $\mathcal L = -\frac1{2} \partial_\mu A_\nu \partial^\mu A^\nu$
\item[] $T^{\mu\nu} = - \partial^\mu A^\rho \partial^\nu A_\rho - \eta^{\mu\nu} \mathcal L$
\item[] $\pi^\mu = \frac{\partial \mathcal L}{\partial (\partial_0 A_\mu)} = - \partial_0 A^\mu \;\Rightarrow\; 
\mathcal H = -\frac1{2} \pi_\mu \pi^\mu - \frac1{2} \partial_i A_\mu \partial_i A^\mu$
\item[] $\Rightarrow [A_\mu(\vec x,t), \pi^\nu(\vec y,t)] = i \delta^\nu_\mu \delta^3(\vec x-\vec y)$
\item[] $A_\mu(\vec x,t) = \int \frac{d^3k}{(2\pi)^3} e^{i \vec k \cdot \vec x} A_\mu(\vec k)$ idem for $\pi^\mu$
\item[] $a_\mu(\vec k) = k_0 A_\mu(\vec k) - i \pi_\mu(\vec k) \Rightarrow [a_\mu(\vec k), a^\dagger_\nu(\vec p)] = -\eta_{\mu\nu} 2k_0 (2\pi)^3 \delta^3(\vec k-\vec p)$
\item[] $H = \int d^3x \mathcal H = -\frac1{2} \eta^{\mu\nu} \int \frac{d^3k}{(2\pi)^3} a^\dagger_\mu(\vec k) a_\nu(\vec k) = - \int d\Omega_k k_0 a^\dagger_\mu(\vec k) a^\mu(\vec k)$
\item[] $P^\mu = - \int d\Omega_k k^\mu a^\dagger_\nu(\vec k) a^\nu(\vec k)$
\item[] $e^{iHt} a_\mu(\vec k) e^{-iHt} = a_\mu(\vec k) e^{-ik_0 t}$ and $a^\dagger$ gets $+ik_0 t$
\item[] $A_\mu(x) = \int d\Omega_k \left( a_\mu(\vec k) e^{-ikx} + a_\mu^\dagger(\vec k) e^{ikx} \right)$
\squishend
